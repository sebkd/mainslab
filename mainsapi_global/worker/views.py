from rest_framework import mixins, status
from rest_framework.parsers import FileUploadParser
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

import pandas as pd

from .inner_methods import handler_address, classifier_of_services, fraud_detector
from .models import Client, Organization, Bill
from .serializers import ClientModelSerializer, \
    BillModelSerializer, BillFileSerializer, ClientFileSerializer


class ClientUploadView(mixins.ListModelMixin,
                       GenericViewSet):
    """Загрузка файла client_org.xlxs
        ожидается POST запрос:
        HEADERS:
        Content-Type: application/vnd.ms-excel
        Content-Disposition: multipart/form-data; filename="client_org.xlsx"
        сам файл в Body
        Endpoint: /api/client_upload
    """
    queryset = []
    serializer_class = ClientModelSerializer
    parser_classes = [FileUploadParser]

    def post(self, request, format=None):
        """Отработка post-запроса"""
        file_serializer = ClientFileSerializer(data=request.data)
        _dict_file_obj = request.data['file'].__dict__
        _excel = _dict_file_obj['_name'].endswith('.xlsx')

        if request.data['file'] is None:
            return Response(
                {"error": "No File Found"},
                status=status.HTTP_400_BAD_REQUEST
            )

        if file_serializer.is_valid():
            if _excel is True:
                data = self.request.data.get('file')
                instances = []

                # отработка листа Клиентов, считается что Клиенты статичны
                xl_client_df = pd.read_excel(data, sheet_name='client')
                columns_client = list(xl_client_df.columns.values)
                name = columns_client[0]
                for index, row in xl_client_df.iterrows():
                    if not Client.objects.filter(name_client=row[name]):
                        instances.append(Client(
                            name_client=row[name],
                        ))
                Client.objects.bulk_create(instances)
                instances.clear()

                # отработка листа Организаций, считается что Организации статичны
                xl_orgs_df = pd.read_excel(data, sheet_name='organization')
                columns_orgs = list(xl_orgs_df.columns.values)
                name_client, name_org, address = columns_orgs[0], columns_orgs[1], columns_orgs[2]
                for index, row in xl_orgs_df.iterrows():
                    if not Organization.objects.filter(name_org=row[name_org]):
                        instances.append(Organization(
                            client_id=Client.objects.filter(name_client=
                                                            row[name_client]).get(name_client=row[name_client]),
                            name_org=row[name_org],
                            address=handler_address(row[address])
                        ))
                Organization.objects.bulk_create(instances)
                instances.clear()
            else:
                return Response(data={"err": "Must be *.xlsx File."},
                                status=status.HTTP_400_BAD_REQUEST
                                )
            file_serializer.save()
            return Response(
                {"message": "Upload Successfull",
                 "data": file_serializer.data}, status=status.HTTP_200_OK)
        else:
            return Response(file_serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST
                            )


class BillsUploadView(
    mixins.ListModelMixin,
    GenericViewSet):
    """Загрузка файла bills.xlxs
        ожидается POST запрос:
        HEADERS:
        Content-Type: application/vnd.ms-excel
        Content-Disposition: multipart/form-data; filename="bills.xlsx"
        сам файл в Body
        Endpoint: /api/bills_upload
    """

    queryset = []
    serializer_class = BillModelSerializer

    parser_classes = [FileUploadParser]

    def post(self, request, format=None):
        """Отработка post-запроса"""
        file_serializer = BillFileSerializer(data=request.data)
        _dict_file_obj = request.data['file'].__dict__
        _excel = _dict_file_obj['_name'].endswith('.xlsx')

        if request.data['file'] is None:
            return Response(
                {"error": "No File Found"},
                status=status.HTTP_400_BAD_REQUEST
            )
        # если файл загрузили ранее файлов с данными по клиентам.
        if not len(Client.objects.all()):
            return Response(
                {"error": "There are no objects for communication, "
                          "first download data on customers and organizations"},
                status=status.HTTP_400_BAD_REQUEST
            )

        if file_serializer.is_valid():
            if _excel is True:
                data = self.request.data.get('file')
                instances = []

                # отработка листа.
                xl_bills_df = pd.read_excel(data)
                columns_bills = list(xl_bills_df.columns.values)
                client_name, organization_name, number, amount, date_bill, service = columns_bills[0], \
                                                                                     columns_bills[1], \
                                                                                     columns_bills[2], \
                                                                                     columns_bills[3], \
                                                                                     columns_bills[4], \
                                                                                     columns_bills[5]

                for index, row in xl_bills_df.iterrows():
                    service_class, service_name = int, str
                    service_dict = classifier_of_services(service_collumn=service)
                    for key, val in service_dict.items():
                        service_class, service_name = key, val
                    instances.append(Bill(
                        organization_id=Organization.objects.filter(name_org=
                                                                    row[organization_name]).get(
                            name_org=row[organization_name]),
                        client_id=Client.objects.filter(name_client=
                                                        row[client_name]).get(name_client=row[client_name]),
                        number=row[number],
                        amount=row[amount],
                        date_bill=row[date_bill],
                        service=row[service],
                        fraud_score=fraud_detector(service_collumn=service),
                        service_class=service_class,
                        service_name=service_name
                    ))
                # делаем через save(), так как .bulk_create - не вызывает сигналы.
                for bill in instances:
                    bill.save()
                instances.clear()

                # заносим суммы из Организаций в Клиенты, так как через сигналы будет геометрическая прогрессия
                # и заносим информацию по количеству организаций, это можно здесь сделать
                for client in Client.objects.all():
                    orgs = Organization.objects.filter(client_id=client.name_client)
                    for org in orgs:
                        client.general_cln_amount += org.general_org_amount
                    client.count_org = len(orgs)
                    client.save()

            else:
                return Response(data={"err": "Must be *.xlsx File."},
                                status=status.HTTP_400_BAD_REQUEST
                                )
            file_serializer.save()
            return Response(
                {"message": "Upload Successfull",
                 "data": file_serializer.data}, status=status.HTTP_200_OK)
        else:
            return Response(file_serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST
                            )


class ClientMixinViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    mixins.ListModelMixin,
    GenericViewSet):
    """
    GET запрос
    endpoint /api/clients
    эндпоинт со списком клиентов
    Данные, которые нужно отдавать для каждого элемента списка:
    - Название клиента
    - Кол-во организаций
    - Приход (сумма по счетам всех организаций клиента)
    """
    queryset = Client.objects.all()
    serializer_class = ClientModelSerializer

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()


class BillMixinViewSet(
    mixins.CreateModelMixin,
    mixins.RetrieveModelMixin,
    mixins.UpdateModelMixin,
    mixins.DestroyModelMixin,
    mixins.ListModelMixin,
    GenericViewSet):
    """
    GET запрос
    endpoint /api/bills
    эндпоинт со списком счетов с возможностью
    фильтровать по организации, клиенту.
    """
    serializer_class = BillModelSerializer

    def perform_destroy(self, instance):
        instance.is_active = False
        instance.save()

    def get_queryset(self):
        """Переопределить queryset с параметрами"""
        try:
            if not self.request.query_params:
                return Bill.objects.all()
            elif 'organization_id' in self.request.query_params:
                queryset = Bill.objects.filter(organization_id=self.request.query_params.get('organization_id'))
                return queryset
            elif 'client_id' in self.request.query_params:
                queryset = Bill.objects.filter(client_id=self.request.query_params.get('client_id'))
                return queryset
        except Exception:
            return Bill.objects.all()
