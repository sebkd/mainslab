from rest_framework import serializers
from rest_framework.serializers import ModelSerializer, HyperlinkedModelSerializer

from .models import BillFileUpload, ClientFileUpload, Client, Bill


class BillFileSerializer(serializers.ModelSerializer):
    """file upload serializer class"""

    class Meta:
        """Contains model & fields used by this serializers"""
        model = BillFileUpload
        fields = '__all__'


class ClientFileSerializer(serializers.ModelSerializer):
    """file upload serializer class"""

    class Meta:
        """Contains model & fields used by this serializers"""
        model = ClientFileUpload
        fields = '__all__'


class ClientModelSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Client
        fields = ['name_client', 'count_org', 'general_cln_amount', ]


class BillModelSerializer(ModelSerializer):
    class Meta:
        model = Bill
        fields = '__all__'
