"""Модуль методов Детектор мошенничества,  Классификатор услуг."""

import random
import re


def handler_address(address: str):
    """
    Проверка на адрес
    :param address:
    :return:
    """
    if isinstance(address, str):
        pattern = r'^\s|^-'
        if re.match(pattern=pattern, string=address):
            return address
        else:
            address = 'Адрес:' + address
            return address
    else:
        return 'No string'


def fraud_detector(service_collumn: str):
    """
    Детектор мошенничества.
    На вход принимает str, на выходе float рандомно в диапазоне от 0 до 1.
    :param service_collumn:
    :return:
    """
    if isinstance(service_collumn, str):
        return random.random()
    else:
        return 'No string'


def classifier_of_services(service_collumn: str):
    """
    Классификатор услуг.
    На вход принимает str, на выходе dict вида {service_class: int, ‘service_name’: str}
    Возвращаемый dict формировать путём рандомного выбора пары ключ-значение из этого
    словаря {1: ‘консультация’, 2: ‘лечение’, 3: ‘стационар’, 4: ‘диагностика’, 5: ‘лаборатория’},
     где ключ это service_class, а значение это ‘service_name’.
    :param service_collumn:
    :return:
    """
    if isinstance(service_collumn, str):
        random_dict = {
            1: 'консультация',
            2: 'лечение',
            3: 'стационар',
            4: 'диагностика',
            5: 'лаборатория'}
        random_key = random.randint(1, 5)
        return {
            random_key: random_dict[random_key]
        }
    else:
        return 'No string'
