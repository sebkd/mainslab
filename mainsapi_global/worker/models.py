"""All model definitions this app"""
from uuid import uuid4

from django.db import models
from django.db.models import signals


class BillFileUpload(models.Model):
    """Represents file upload model class."""
    file = models.FileField(upload_to='xls_uploads/bill/%y/%m')
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        """Return file name"""
        return self.file.name


class ClientFileUpload(models.Model):
    """Represents file upload model class."""
    file = models.FileField(upload_to='xls_uploads/client/%y/%m')
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        """Return file name"""
        return self.file.name


class Client(models.Model):
    """Класс клиентов"""

    name_client = models.CharField(max_length=256, unique=True, verbose_name='Клиент', primary_key=True)
    created = models.DateTimeField(auto_now_add=True, serialize=False)
    is_active = models.BooleanField(default=True)
    count_org = models.PositiveIntegerField(verbose_name='Количество организаций',
                                            blank=True,
                                            default=0)
    general_cln_amount = models.FloatField(verbose_name='Общая сумма по клиенту',
                                           blank=True,
                                           default=0)

    def __str__(self):
        return self.name_client


class Organization(models.Model):
    """Класс организаций"""

    client_id = models.ForeignKey(Client, on_delete=models.CASCADE, null=True, verbose_name='Клиент')
    name_org = models.CharField(verbose_name='Организация', max_length=256, primary_key=True)
    address = models.TextField(verbose_name='Адрес', blank=True)
    fraud_weight = models.IntegerField(verbose_name='Вес',
                                       serialize=True,
                                       default=0
                                       )
    general_org_amount = models.FloatField(verbose_name='Общая сумма по организации',
                                           blank=True,
                                           default=0)
    created = models.DateTimeField(auto_now_add=True, serialize=False)
    is_active = models.BooleanField(default=True)

    class Meta:
        unique_together = ('client_id', 'name_org',)


class Bill(models.Model):
    """Счета организаций"""
    uid = models.UUIDField(primary_key=True, default=uuid4, auto_created=True, serialize=False)
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE, null=True, verbose_name='Клиент')
    organization_id = models.ForeignKey(Organization, on_delete=models.CASCADE, null=True, verbose_name='Организация')
    number = models.IntegerField(verbose_name='номер счета')
    amount = models.FloatField(verbose_name='сумма счета')
    date_bill = models.DateField(verbose_name='дата счета')
    service = models.TextField()
    fraud_score = models.FloatField(verbose_name='от 0 до 1 рандомно, через детектор мошенничества',
                                    blank=True)
    service_class = models.IntegerField(verbose_name='выход ключа из классификатора услуг',
                                        blank=True)
    service_name = models.CharField(verbose_name='выход значения из классификатора услуг',
                                    max_length=256,
                                    blank=True)
    created = models.DateTimeField(auto_now_add=True, serialize=False)
    is_active = models.BooleanField(default=True)

    class Meta:
        unique_together = ('organization_id', 'number',)


def bill_pre_save(sender, instance, signal, *args, **kwargs):
    """Проверка счета на 0.9 и передача данных по сумма по счетам"""

    org = Organization.objects.all().get(name_org=instance.organization_id.name_org)
    if not instance.fraud_score < 0.9:
        org.fraud_weight += 1
    org.general_org_amount += instance.amount
    org.save()


signals.pre_save.connect(bill_pre_save, sender=Bill)
